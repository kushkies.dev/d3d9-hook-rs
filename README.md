### Code is hosted at github now https://github.com/kushkies/d3d9-hook-rs

Detour crate requires nightly rust.

D3D9 Hook example. Example renders an filled box.

![image example](./example.png)

Note: There doesn't appear to be bindings for d3d9 fonts.  Might need to look into alternatives

Credits: [Guided Hacking](https://guidedhacking.com/) [GH D3D9 Hook Tutorial](https://guidedhacking.com/threads/d3d9-endscene-hook-template-using-dummy-device.14008/)
